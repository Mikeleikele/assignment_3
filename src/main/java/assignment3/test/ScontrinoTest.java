package assignment3.test;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import assignment3.dao.ProductDao;
import assignment3.dao.PromotionDao;
import assignment3.dao.ScontrinoDao;
import assignment3.dao.UserDao;
import assignment3.entities.Scontrino;

import static org.junit.Assert.*;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ScontrinoTest {

	private static final EntityManager entityManager = Persistence.createEntityManagerFactory("pss_assignment3").createEntityManager();
		
	public static void dropDB() {
        entityManager.close();
	}
			
	@BeforeClass
	public static void setUp() {
		assertNotNull(entityManager);
	}
	
		@Test
	    public void test1() throws Exception{

	    	ScontrinoDao.create(1, 10);
	    	ScontrinoDao.create(2, 20);
	    	ScontrinoDao.create(3, 30);
	    	ScontrinoDao.create(4, 40);
	    	ScontrinoDao.create(5, 50);
	    	ScontrinoDao.create(6, 50);
	    	ScontrinoDao.create(7, 50);
	    	ScontrinoDao.create(8, 50);
	    	//test find
	    	assertEquals(2,ScontrinoDao.findById(2).getId());
	    	assertEquals(null,ScontrinoDao.findById(16));
	    	
	    	Scontrino scontrino1 = ScontrinoDao.findById(1);
	    	Scontrino scontrino2 = ScontrinoDao.findById(2);
	    	Scontrino scontrino3 = ScontrinoDao.findById(3);
	    	assertNotNull(scontrino1);
	    	assertNotNull(scontrino2);
	    	assertNotNull(scontrino3);
	    	
	    	//test create
	    	assertEquals(2,scontrino2.getId());
	    	assertEquals(20,scontrino2.getTotal());
	    	
	    	//test update
	    	ScontrinoDao.update(1, 100);
	    	scontrino1 = ScontrinoDao.findById(1);
	    	
	    	assertEquals(1,scontrino1.getId());
	    	assertEquals(100,scontrino1.getTotal());

	    	//delete
	    	
	    	int howManyCardBeforeDeletion = ScontrinoDao.readAll().size();	    	
	    	assertEquals(8,howManyCardBeforeDeletion);
	    	ScontrinoDao.delete(8);
	    	int howManyCardBeforeAfterDeletion = ScontrinoDao.readAll().size();
	    	
	       	assertEquals(7,howManyCardBeforeAfterDeletion);
	    	assertEquals(null,ScontrinoDao.findById(8));    	
	    }
		
		
		@Test
	    public void test2() throws Exception{
	    	ProductDao.create(1, "product1", "description1");
	    	ProductDao.create(2, "product2", "description2");
	    	ProductDao.create(3, "product3", "description3");
	    	ProductDao.create(4, "product4", "description4");
	    	ProductDao.create(5, "product5", "description5");
	    	ProductDao.create(6, "product6", "description6");
	    	
	    	ProductDao.addProduct(1, ProductDao.findById(2));
			ProductDao.addParent(2, ProductDao.findById(1));	
			ProductDao.addProduct(1, ProductDao.findById(3));
			ProductDao.addParent(3, ProductDao.findById(1));
			ProductDao.addProduct(4, ProductDao.findById(5));
			ProductDao.addParent(5, ProductDao.findById(4));	
			ProductDao.addProduct(4, ProductDao.findById(6));
			ProductDao.addParent(6, ProductDao.findById(4));				
			
			ScontrinoDao.addProduct(1, ProductDao.findById(1));
			ProductDao.addScontrino(1, ScontrinoDao.findById(1));			
			
			ScontrinoDao.addProduct(2, ProductDao.findById(4));
			ProductDao.addScontrino(4, ScontrinoDao.findById(2));
			
			ScontrinoDao.addProduct(3, ProductDao.findById(5));
			ProductDao.addScontrino(5, ScontrinoDao.findById(3));
			
			// Remove scontrino3 with Product5 that is Product4 child
			ScontrinoDao.delete(3);
			assertEquals(6,ScontrinoDao.readAll().size());
			assertEquals(5,ProductDao.readAll().size());
			assertEquals(null,ProductDao.findById(5));
			assertEquals(1,ProductDao.findById(4).getListProduct().size());
			assertEquals(4,ProductDao.findById(6).getParent().getId());

			// Remove scontrino2 with Product4 that have Product6 as child
			ScontrinoDao.delete(2);
			assertEquals(5,ScontrinoDao.readAll().size());
			assertEquals(3,ProductDao.readAll().size());
			assertEquals(null,ProductDao.findById(4));
			assertEquals(null,ProductDao.findById(6));
		}

		@Test
	    public void test3() throws Exception{
	    	
			PromotionDao.create(1, "name1", 10);
	    	PromotionDao.create(2, "name2", 20);
	    	
	    	ScontrinoDao.addPromotion(1, PromotionDao.findById(1));
	    	PromotionDao.addOrder(1, ScontrinoDao.findById(1));
	    	
	    	ScontrinoDao.addPromotion(4, PromotionDao.findById(1));
	    	PromotionDao.addOrder(1, ScontrinoDao.findById(4));
	    	
	    	ScontrinoDao.addPromotion(5, PromotionDao.findById(2));
	    	PromotionDao.addOrder(2, ScontrinoDao.findById(5));
	    	
	    			
			// Remove scontrino3 with Product5 that is Product4 child
			ScontrinoDao.delete(5);
			assertEquals(4,ScontrinoDao.readAll().size());
			assertEquals(2,PromotionDao.readAll().size());
			assertEquals(0,PromotionDao.findById(2).getListOrder().size());
			assertEquals(1,ScontrinoDao.findById(4).getPromotion().getId());
			assertEquals(2,PromotionDao.findById(1).getListOrder().size());

			// Remove scontrino2 with Product4 that have Product6 as child
			ScontrinoDao.delete(4);
			assertEquals(3,ScontrinoDao.readAll().size());
			assertEquals(2,PromotionDao.readAll().size());
			assertEquals(1,PromotionDao.findById(1).getListOrder().size());
			assertEquals(1,ScontrinoDao.findById(1).getPromotion().getId());			
		}
			

		@Test
	    public void test4() throws Exception{
	    	
			UserDao.create(1, "mail1", "name1", "surname1", "city1", "street1", "civicNumber1");
	    	UserDao.create(2, "mail2", "name2", "surname2", "city2", "street2", "civicNumber2");
	    	
	    	UserDao.addOrder(1, ScontrinoDao.findById(7));
	    	ScontrinoDao.addUser(1, UserDao.findById(1));
	    	
	    	UserDao.addOrder(2, ScontrinoDao.findById(6));
	    	ScontrinoDao.addUser(6, UserDao.findById(2));
	    	
	    	UserDao.addOrder(2, ScontrinoDao.findById(7));
	    	ScontrinoDao.addUser(7, UserDao.findById(2));
	    	
	    			
			// Remove scontrino3 with Product5 that is Product4 child
			ScontrinoDao.delete(6);
			assertEquals(2,ScontrinoDao.readAll().size());
			assertEquals(2,UserDao.readAll().size());
			assertEquals(2,ScontrinoDao.findById(7).getUser().getId());
			
			// Remove scontrino2 with Product4 that have Product6 as child
			UserDao.delete(2);
			assertEquals(1,ScontrinoDao.readAll().size());
			assertEquals(null,ScontrinoDao.findById(7));			
		}
		
		@Test
	    public void test5() throws Exception{
			UserDao.delete(1);
			assertEquals(0,ScontrinoDao.readAll().size());
			assertEquals(2,PromotionDao.readAll().size());
			assertEquals(0,ProductDao.readAll().size());
		}
		
		@AfterClass
	    public static void after(){
	    	dropDB();
	    }
}