package assignment3.test;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import assignment3.dao.CardDao;
import assignment3.dao.ScontrinoDao;
import assignment3.dao.UserDao;
import assignment3.entities.Card;
import assignment3.entities.Scontrino;
import assignment3.entities.User;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserTest {

	private static final EntityManager entityManager = Persistence.createEntityManagerFactory("pss_assignment3").createEntityManager();
		
	public static void dropDB() {
        entityManager.close();
	}
			
	@BeforeClass
	public static void setUp() {
		//dropDB();
		assertNotNull(entityManager);
	}
	
		@Test
	    public void test1() throws Exception{

	    	User user1;
			User user2;
			User user3;
			User user4;
	    	UserDao.create(1, "mail1", "name1", "surname1", "city1", "street1", "civicNumber1");
	    	UserDao.create(2, "mail2", "name2", "surname2", "city2", "street2", "civicNumber2");
	    	UserDao.create(3, "mail3", "name3", "surname3", "city3", "street3", "civicNumber3");
	    	UserDao.create(4, "mail4", "name4", "surname4", "city4", "street4", "civicNumber4");
	    	
	    	//test find
	    	//test find
	    	assertEquals(2,UserDao.findById(2).getId());
	    	assertEquals(null,UserDao.findById(12));
	    	
	    	user1 = UserDao.findById(1);
	    	user2 = UserDao.findById(2);
	    	user3 = UserDao.findById(3);
	    	user4 = UserDao.findById(4);
	    	
	    	assertNotNull(user1);
	    	assertNotNull(user2);
	    	assertNotNull(user3);
	    	assertNotNull(user4);
	    	
	    	//test create
	    	assertEquals(2,user2.getId());
	    	assertEquals("mail2",user2.getMail());
	    	assertEquals("name2",user2.getName());
	    	assertEquals("surname2",user2.getSurname());
	    	assertEquals("city2",user2.getCity());
	    	assertEquals("street2",user2.getStreet());
	    	assertEquals("civicNumber2",user2.getCivicNumber());
	    	
	    	//test update
	    	UserDao.update(1, "new_mail1", "new_name1", "new_surname1", "new_city1", "new_street1", "new_civicNumber1");
	    	user1 = UserDao.findById(1);
	    	
	    	assertEquals("new_mail1",user1.getMail());
	    	assertEquals("new_name1",user1.getName());
	    	assertEquals("new_surname1",user1.getSurname());
	    	assertEquals("new_city1",user1.getCity());
	    	assertEquals("new_street1",user1.getStreet());
	    	assertEquals("new_civicNumber1",user1.getCivicNumber());
	    	
	    	
	    	UserDao.updateAddress(2, "new_city2", "new_street2", "new_civicNumber2");
	    	user2 = UserDao.findById(2);
	    	
	    	assertEquals("new_city2",user2.getCity());
	    	assertEquals("new_street2",user2.getStreet());
	    	assertEquals("new_civicNumber2",user2.getCivicNumber());

	    	//delete
	    	List<User> listUser;
	    	
	    	UserDao.delete(4);
	    	listUser = UserDao.readAll();
	    	assertEquals(3,listUser.size());
	    	
	    	UserDao.deleteAll();
	    	listUser = UserDao.readAll();
	    	assertEquals(0,listUser.size());
	    	
	    }
		
		
		@Test
	    public void test2() throws Exception{
	    	
	    	UserDao.create(5, "mail1", "name1", "surname1", "city1", "street1", "civicNumber1");
	    	UserDao.create(6, "mail2", "name2", "surname2", "city2", "street2", "civicNumber2");
	    	UserDao.create(7, "mail3", "name3", "surname3", "city3", "street3", "civicNumber3");
	    	UserDao.create(8, "mail4", "name4", "surname4", "city4", "street4", "civicNumber4");
	    	

	    	
	    	CardDao.create(1, "card1", 1);
	    	CardDao.create(2, "card2", 2);
	    	CardDao.create(3, "card3", 3);
	    	
	    	
	    	List<Card> cards;
	    	cards = CardDao.readAll();
	    	assertEquals(3,cards.size());
	    	
	    	Card card1 = CardDao.findById(1);
	    	assertEquals(1,card1.getId());
	    	Card card2 = CardDao.findById(2);
	    	Card card3 = CardDao.findById(3);
	    	
	    	// test add
	    	UserDao.addCard(8, card1);
	    	CardDao.addUser(1, UserDao.findById(8));
	    	
	    	UserDao.addCard(8, card2);
	    	CardDao.addUser(2, UserDao.findById(8));
	    	
	    	UserDao.addCard(6, card3);
	    	CardDao.addUser(3, UserDao.findById(6));
	    	
	    	Set<Card> cards1 = UserDao.findById(5).getListCard();
	    	Set<Card> cards2 = UserDao.findById(6).getListCard();
	    	Set<Card> cards3 = UserDao.findById(7).getListCard();
	    	Set<Card> cards4 = UserDao.findById(8).getListCard();
	   
	    	assertEquals(0,cards1.size());
	    	assertEquals(1,cards2.size());
	    	assertEquals(0,cards3.size());
	    	assertEquals(2,cards4.size());
	    	
	    	// delete
	    	int cardQuantityBeforeDeletion = CardDao.readAll().size();
	    	UserDao.delete(8);
	    	User _deletedUser = UserDao.findById(8);
	    	assertEquals(null,_deletedUser);
	    	int cardQuantityAfterDeletion = CardDao.readAll().size();
	    	assertEquals(cardQuantityAfterDeletion,cardQuantityBeforeDeletion-2);
	    	assertEquals(null,CardDao.findById(1));
	    	assertEquals(null,CardDao.findById(2));
	    	assertNotEquals(null,CardDao.findById(3));	    	
		}
		
	
		@Test
	    public void test3() throws Exception{
	    	
	    	
	    	
    	
	    	ScontrinoDao.create(1, 10);
	    	ScontrinoDao.create(2, 10);
	    	ScontrinoDao.create(3, 10);
	    	
	    	List<Scontrino> scontrini = ScontrinoDao.readAll();
	    	assertEquals(3,scontrini.size());
	    	
	    	Scontrino scontrino1 = ScontrinoDao.findById(1);
	    	assertEquals(1,scontrino1.getId());
	    	Scontrino scontrino2 = ScontrinoDao.findById(2);
	    	Scontrino scontrino3 = ScontrinoDao.findById(3);

	    	UserDao.addOrder(5, scontrino1);
	    	ScontrinoDao.addUser(1, UserDao.findById(5));
	    	
	    	UserDao.addOrder(5, scontrino2);
	    	ScontrinoDao.addUser(2, UserDao.findById(5));
	    	
	    	UserDao.addOrder(6, scontrino3);
	    	ScontrinoDao.addUser(3, UserDao.findById(6));
	    		    	
	    	assertEquals(2,UserDao.findById(5).getListOrder().size());
	    	assertEquals(1,UserDao.findById(6).getListOrder().size());
	    	assertEquals(0,UserDao.findById(7).getListOrder().size());
	    	
	    	// delete
	    	int cardQuantityBeforeDeletion = ScontrinoDao.readAll().size();
	    	UserDao.delete(5);
	    	User _deletedUser = UserDao.findById(5);
	    	assertEquals(null,_deletedUser);
	    	
	    	int cardQuantityAfterDeletion = ScontrinoDao.readAll().size();
	    	assertEquals(cardQuantityBeforeDeletion-2,cardQuantityAfterDeletion);
	    	assertEquals(null,ScontrinoDao.findById(1));
	    	assertEquals(null,ScontrinoDao.findById(2));
	    	assertNotEquals(null,ScontrinoDao.findById(3));
	    	
	    	// delete with card
	    	assertNotEquals(null,CardDao.findById(3));
	    	int howManyCardUser6 = UserDao.findById(6).getListOrder().size();
	    	assertEquals(1,howManyCardUser6);
	    	 cardQuantityBeforeDeletion = ScontrinoDao.readAll().size();
	    	UserDao.delete(6);
	    	_deletedUser = UserDao.findById(6);
	    	assertEquals(null,_deletedUser);
	    	
	    	cardQuantityAfterDeletion = ScontrinoDao.readAll().size();
	    	assertEquals(cardQuantityBeforeDeletion-1,cardQuantityAfterDeletion);
	    	assertEquals(null,ScontrinoDao.findById(3));
	    	assertEquals(null,CardDao.findById(3));
		}
	
		@AfterClass
	    public static void after(){
	    	dropDB();
	    }
}
