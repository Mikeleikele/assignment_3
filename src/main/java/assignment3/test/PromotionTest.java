package assignment3.test;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;


import assignment3.dao.PromotionDao;
import assignment3.dao.ScontrinoDao;
import assignment3.entities.Promotion;

import static org.junit.Assert.*;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PromotionTest {

	private static final EntityManager entityManager = Persistence.createEntityManagerFactory("pss_assignment3").createEntityManager();
		
	public static void dropDB() {
        entityManager.close();
	}
			
	@BeforeClass
	public static void setUp() {
		assertNotNull(entityManager);
	}
	
		@Test
	    public void test1() throws Exception{

	    	PromotionDao.create(1, "name1", 10);
	    	PromotionDao.create(2, "name2", 20);
	    	PromotionDao.create(3, "name3", 30);
	    	PromotionDao.create(4, "name4", 40);
	    	
	    	//test find
	    	assertEquals(2,PromotionDao.findById(2).getId());
	    	assertEquals(null,PromotionDao.findById(5),null);
	    	
	    	Promotion card1 = PromotionDao.findById(1);
	    	Promotion card2 = PromotionDao.findById(2);
	    	Promotion card3 = PromotionDao.findById(3);
	    	assertNotNull(card1);
	    	assertNotNull(card2);
	    	assertNotNull(card3);
	    	
	    	//test create
	    	assertEquals(2,card2.getId(),2);
	    	assertEquals("name2",card2.getName());
	    	assertEquals(20,card2.getDiscount(),20);
	    	
	    	//test update
	    	PromotionDao.update(1, "new_code1", 100);
	    	card1 = PromotionDao.findById(1);
	    	
	    	assertEquals(1,card1.getId());
	    	assertEquals("new_code1",card1.getName());
	    	assertEquals(100,card1.getDiscount());

	    	//delete
	    	
	    	int howManyCardBeforeDeletion = PromotionDao.readAll().size();	    	
	    	assertEquals(4,howManyCardBeforeDeletion,4);
	    	PromotionDao.delete(4);
	    	int howManyCardBeforeAfterDeletion = PromotionDao.readAll().size();
	    	
	       	assertEquals(howManyCardBeforeDeletion-1,howManyCardBeforeAfterDeletion);
	    	assertEquals(null,PromotionDao.findById(4));    	
	    }
		
		
		@Test
	    public void test2() throws Exception{
	    	
			ScontrinoDao.create(1,10);
			ScontrinoDao.create(2,20);
			ScontrinoDao.create(3,30);

	       	assertEquals(3,ScontrinoDao.readAll().size());
	       	assertEquals(3,PromotionDao.readAll().size());
	    	
	    	
	    	// test add
	    	PromotionDao.addOrder(1, ScontrinoDao.findById(1));
	    	ScontrinoDao.addPromotion(1, PromotionDao.findById(1));
	    	
	    	PromotionDao.addOrder(1, ScontrinoDao.findById(2));
	    	ScontrinoDao.addPromotion(2, PromotionDao.findById(1));
	    	
	    	PromotionDao.addOrder(2, ScontrinoDao.findById(3));
	    	ScontrinoDao.addPromotion(3, PromotionDao.findById(2));
	    	
	    	assertEquals(1,ScontrinoDao.findById(1).getPromotion().getId());
	    	assertEquals(1,ScontrinoDao.findById(2).getPromotion().getId());
	    	assertEquals(2,ScontrinoDao.findById(3).getPromotion().getId());
	    	
	    	assertEquals(2,PromotionDao.findById(1).getListOrder().size());
	    	// delete
	    	int scontriniQuantityBeforeDeletion = ScontrinoDao.readAll().size();
	    	int scontriniQuantityPromo1 = PromotionDao.findById(1).getListOrder().size();
	    	PromotionDao.delete(1);
	    	assertEquals(null,PromotionDao.findById(1));
	    	assertEquals(null,ScontrinoDao.findById(1));
	    	assertEquals(null,ScontrinoDao.findById(2));
	    	assertNotEquals(null,ScontrinoDao.findById(3));
	    	
	    		    	
	    	int scontriniQuantityAfterDeletion = ScontrinoDao.readAll().size();
	    	
	    	assertEquals(scontriniQuantityBeforeDeletion-scontriniQuantityPromo1,scontriniQuantityAfterDeletion);
	    	
	    	PromotionDao.deleteAll();
	    	assertEquals(0,ScontrinoDao.readAll().size());
	    	assertEquals(0,PromotionDao.readAll().size());
	    	
		}
	
		@AfterClass
	    public static void after(){
	    	dropDB();
	    }
}
