package assignment3.test;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import assignment3.dao.CardDao;
import assignment3.dao.UserDao;
import assignment3.entities.Card;
import assignment3.entities.User;

import static org.junit.Assert.*;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CardTest {

	private static final EntityManager entityManager = Persistence.createEntityManagerFactory("pss_assignment3").createEntityManager();
		
	public static void dropDB() {
        entityManager.close();
	}
			
	@BeforeClass
	public static void setUp() {
		assertNotNull(entityManager);
	}
	
		@Test
	    public void test1() throws Exception{

	    	CardDao.create(1, "code1", 10);
	    	CardDao.create(2, "code2", 20);
	    	CardDao.create(3, "code3", 30);
	    	CardDao.create(4, "code4", 40);
	    	
	    	//test find
	    	assertEquals(2,CardDao.findById(2).getId());
	    	assertEquals(null,CardDao.findById(5));
	    	
	    	Card card1 = CardDao.findById(1);
	    	Card card2 = CardDao.findById(2);
	    	Card card3 = CardDao.findById(3);
	    	assertNotNull(card1);
	    	assertNotNull(card2);
	    	assertNotNull(card3);
	    	
	    	//test create
	    	assertEquals(2,card2.getId());
	    	assertEquals("code2",card2.getCode());
	    	assertEquals(20,card2.getPoints());
	    	
	    	//test update
	    	CardDao.update(1, "new_code1", 100);
	    	card1 = CardDao.findById(1);
	    	
	    	assertEquals(1,card1.getId());
	    	assertEquals("new_code1",card1.getCode());
	    	assertEquals(100,card1.getPoints());

	    	//delete
	    	
	    	int howManyCardBeforeDeletion = CardDao.readAll().size();	    	
	    	assertEquals(4,howManyCardBeforeDeletion);
	    	CardDao.delete(4);
	    	int howManyCardBeforeAfterDeletion = CardDao.readAll().size();
	    	
	       	assertEquals(howManyCardBeforeDeletion-1,howManyCardBeforeAfterDeletion);
	    	assertEquals(null,CardDao.findById(4));    	
	    }
		
		
		@Test
	    public void test2() throws Exception{
	    	
			UserDao.create(1, "mail1", "name1", "surname1", "city1", "street1", "civicNumber1");
	    	UserDao.create(2, "mail2", "name2", "surname2", "city2", "street2", "civicNumber2");
	    	UserDao.create(3, "mail3", "name3", "surname3", "city3", "street3", "civicNumber3");
	    	UserDao.create(4, "mail4", "name4", "surname4", "city4", "street4", "civicNumber4");    	

	       	assertEquals(3,CardDao.readAll().size());
	    	
	    	Card card1 = CardDao.findById(1);
	    	assertEquals(1,card1.getId());
	    	Card card2 = CardDao.findById(2);
	    	Card card3 = CardDao.findById(3);
	    	
	    	// test add
	    	UserDao.addCard(1, card1);
	    	CardDao.addUser(1, UserDao.findById(1));
	    	
	    	UserDao.addCard(1, card2);
	    	CardDao.addUser(2, UserDao.findById(1));
	    	
	    	UserDao.addCard(2, card3);
	    	CardDao.addUser(3, UserDao.findById(2));	    	
	   
	    	assertEquals(1,CardDao.findById(1).getUser().getId());
	    	assertEquals(1,CardDao.findById(2).getUser().getId());
	    	assertEquals(2,CardDao.findById(3).getUser().getId());
	    	
	    	
	    	// delete
	    	int cardQuantityBeforeDeletion = UserDao.findById(1).getListCard().size();
	    	CardDao.delete(1);
	    	assertEquals(null,CardDao.findById(1));
	    	
	    	User _selectUser = UserDao.findById(1);
	    	assertNotEquals(null,_selectUser);
	    	
	    	int cardQuantityAfterDeletion = UserDao.findById(1).getListCard().size();
	    	
	    	assertEquals(cardQuantityBeforeDeletion-1,cardQuantityAfterDeletion);
	    	
	    	CardDao.deleteAll();
	    	assertEquals(4,UserDao.readAll().size());
	    	assertEquals(0,CardDao.readAll().size());
	    	
		}
			
		@AfterClass
	    public static void after(){
	    	dropDB();
	    }
}