package assignment3.test;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import assignment3.dao.ProductDao;
import assignment3.dao.ScontrinoDao;
import assignment3.entities.Product;

import static org.junit.Assert.*;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProductTest {

	private static final EntityManager entityManager = Persistence.createEntityManagerFactory("pss_assignment3").createEntityManager();
		
	public static void dropDB() {
        entityManager.close();
	}
			
	@BeforeClass
	public static void setUp() {
		assertNotNull(entityManager);
	}
	
		@Test
	    public void test1() throws Exception{

	    	ProductDao.create(1, "name1", "description1");
	    	ProductDao.create(2, "name2", "description2");
	    	ProductDao.create(3, "name3", "description3");
	    	ProductDao.create(4, "name4", "description4");
	    	ProductDao.create(5, "name5", "description5");
	    	ProductDao.create(6, "name6", "description6");
	    	ProductDao.create(7, "name7", "description7");
	    	//test find
	    	assertEquals(2,ProductDao.findById(2).getId());
	    	assertEquals(null,ProductDao.findById(8));
	    	
	    	Product product1 = ProductDao.findById(1);
	    	Product product2 = ProductDao.findById(2);
	    	Product product3 = ProductDao.findById(3);
	    	assertNotNull(product1);
	    	assertNotNull(product2);
	    	assertNotNull(product3);
	    	
	    	//test create
	    	assertEquals(2,product2.getId());
	    	assertEquals("name2",product2.getName());
	    	assertEquals("description2",product2.getDescription());
	    	
	    	//test update
	    	ProductDao.update(1, "new_name1", "new_description1");
	    	product1 = ProductDao.findById(1);
	    	
	    	assertEquals(1,product1.getId());
	    	assertEquals("new_name1",product1.getName());
	    	assertEquals("new_description1",product1.getDescription());

	    	//delete
	    	
	    	int howManyCardBeforeDeletion = ProductDao.readAll().size();	    	
	    	assertEquals(7,howManyCardBeforeDeletion);
	    	ProductDao.delete(7);
	    	int howManyCardBeforeAfterDeletion = ProductDao.readAll().size();
	       	assertEquals(6,howManyCardBeforeAfterDeletion);
	    	assertEquals(null,ProductDao.findById(7));    	
	    }
		
		
		@Test
	    public void test2() throws Exception{
	    	
			assertEquals(6,ProductDao.readAll().size());
	    	
	    		    	
	    	// test add
			ProductDao.addProduct(1, ProductDao.findById(2));
			ProductDao.addParent(2, ProductDao.findById(1));	
			
			ProductDao.addProduct(1, ProductDao.findById(3));
			ProductDao.addParent(3, ProductDao.findById(1));
			
			ProductDao.addProduct(4, ProductDao.findById(5));
			ProductDao.addParent(5, ProductDao.findById(4));	
			
			ProductDao.addProduct(4, ProductDao.findById(6));
			ProductDao.addParent(6, ProductDao.findById(4));
			
			assertEquals(2,ProductDao.findById(1).getListProduct().size());
			assertEquals(2,ProductDao.findById(4).getListProduct().size());
	    	
			assertEquals(1,ProductDao.findById(2).getParent().getId());
			assertEquals(1,ProductDao.findById(3).getParent().getId());
			
			assertEquals(4,ProductDao.findById(5).getParent().getId());
			assertEquals(4,ProductDao.findById(5).getParent().getId());    	
	    	
	    	// delete child
	    	int cardQuantityBeforeDeletion = ProductDao.readAll().size();
	    	ProductDao.delete(6);
	    	int cardQuantityAfterDeletion = ProductDao.readAll().size();

	    	assertEquals(null,ProductDao.findById(6));
	    	assertNotEquals(null,ProductDao.findById(5));
	    	assertNotEquals(null,ProductDao.findById(4));
	    	assertEquals(4,ProductDao.findById(5).getParent().getId());
	    	assertEquals(6,cardQuantityBeforeDeletion);
	    	assertEquals(5,cardQuantityAfterDeletion);
	    	
	    	// delete parent
	    	cardQuantityBeforeDeletion = ProductDao.readAll().size();
	    	ProductDao.delete(4);
	    	cardQuantityAfterDeletion = ProductDao.readAll().size();

	    	
	    	assertEquals(null,ProductDao.findById(5));
	    	assertEquals(null,ProductDao.findById(4));
	    	assertEquals(5,cardQuantityBeforeDeletion);
	    	assertEquals(3,cardQuantityAfterDeletion); 		    	
		}
		
		@Test
		public void test3() throws Exception{
	    	
			ScontrinoDao.create(1, 10);
			ScontrinoDao.create(2, 20);
			ScontrinoDao.create(3, 30);
			
			ScontrinoDao.addProduct(1, ProductDao.findById(1));
			ProductDao.addScontrino(1, ScontrinoDao.findById(1));
			
			ScontrinoDao.addProduct(1, ProductDao.findById(2));
			ProductDao.addScontrino(2, ScontrinoDao.findById(1));
			
			ScontrinoDao.addProduct(2, ProductDao.findById(3));
			ProductDao.addScontrino(3, ScontrinoDao.findById(2));
			
			assertEquals(2,ScontrinoDao.findById(1).getListProduct().size());
			assertEquals(1,ScontrinoDao.findById(2).getListProduct().size());
			assertEquals(0,ScontrinoDao.findById(3).getListProduct().size());
			
			// product3 hasm't the childs
			ProductDao.delete(3);
			assertEquals(3,ScontrinoDao.readAll().size());
			assertNotEquals(null,ScontrinoDao.findById(2));
			assertEquals(2,ScontrinoDao.findById(1).getListProduct().size());
			assertEquals(0,ScontrinoDao.findById(2).getListProduct().size());
			
			
			// product1 has the child: product2
			ProductDao.delete(1);
			assertEquals(3,ScontrinoDao.readAll().size());
			assertNotEquals(null,ScontrinoDao.findById(1));						
			assertEquals(0,ScontrinoDao.findById(1).getListProduct().size());
			assertEquals(0,ScontrinoDao.findById(2).getListProduct().size());
			
		}
			
		@AfterClass
	    public static void after(){
	    	dropDB();
	    }
}
