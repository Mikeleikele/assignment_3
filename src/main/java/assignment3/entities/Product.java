package assignment3.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "product")
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "product_id")
	private int id;
	
	@Column(name = "name")
	private String name  ;
	
	@Column(name = "description")
	private String description;
	
	@ManyToOne
    private Product parent;
	public void setParent(Product parent) {
		this.parent = parent;
	}
	public Product getParent() {
		return this.parent;
	}
	
	@OneToMany(mappedBy="parent", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE, targetEntity = Product.class, orphanRemoval = true)   
    private Set<Product> children = new HashSet<Product>();    
    public void addProduct(Product product) {
		this.children.add(product);
	}	
	public Set<Product> getListProduct() {
		return this.children;
	}
	
	@ManyToOne
    @JoinColumn(name = "order_id")
    private Scontrino scontrino;
	public void setScontrino(Scontrino scontrino) {
		this.scontrino = scontrino;
	}
	public Scontrino getScontrino() {
		return this.scontrino;
	}
		
	public Product(int id, String name, String ingradients) {
		this.id = id;
		this.name = name;
		this.description = ingradients;	
		this.scontrino = null;
		this.parent = null;
	}

	public Product() {		
	}
	
	// id
	public void setId(int id) {
		this.id = id;
	}
	public int getId() {
		return this.id;
	}

	// name
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return this.name;
	}
	
	// discount
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDescription() {
		return this.description;
	}
	
	@Override
	public String toString() {
		return "Product [" + "id=" + this.id + ", name=" + this.name + ", description=" + this.description + "]";
	}
}