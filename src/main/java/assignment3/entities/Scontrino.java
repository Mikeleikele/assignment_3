package assignment3.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "scontrino")
public class Scontrino {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "order_id")
	private int id;
	
	@Column(name = "total")
	private int total;
		
	// define cascade
	@ManyToOne//(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.REMOVE})
	@JoinColumn(name = "user_id")
    private User user;
	public void setUser(User user) {
		this.user = user;
	}
	public User getUser() {
		return this.user;
	}
	
	@ManyToOne //(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.REMOVE})
    @JoinColumn(name = "promotion_id")
    private Promotion promotion;
	public void setPromotion(Promotion promotionUsed) {
		this.promotion = promotionUsed;
	}
	public Promotion getPromotion() {
		return this.promotion;
	}
	
	@OneToMany(mappedBy="scontrino",cascade = CascadeType.REMOVE, fetch  = FetchType.EAGER,  targetEntity = Product.class, orphanRemoval = true )
    private Set<Product> scontrino_product = new HashSet<Product>();	
	public void addProduct(Product product) {
		this.scontrino_product.add(product);
	}	
	public Set<Product> getListProduct() {
		return this.scontrino_product;
	}
	
	public Scontrino(int id, int total) {		
		this.id = id;
		this.total = total;	
		user = null;
		promotion = null;
	}

	public Scontrino() {
		user = null;
		promotion = null;
	}
	
	// id
	public void setId(int id) {
		this.id = id;
	}
	public int getId() {
		return this.id;
	}

	// total
	public void setTotal(int total) {
		this.total = total;
	}
	public int getTotal() {
		return this.total;
	}
	
	@Override
	public String toString() {
		return "Order [" + "id=" + this.id + ", total=" + this.total + "]";
	}
}