package assignment3.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "card")
public class Card {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "card_id")
	private int id;
	
	@Column(name = "code")
	private String code  ;
	
	@Column(name = "points")
	private int points;
	
	@ManyToOne
	@JoinColumn(name = "user_id")
    private User user;
	public void setUser(User user) {
		this.user = user;
	}
	public User getUser() {
		return this.user;
	}	
	
	public Card(int id, String code, int points) {
		this.id = id;
		this.code = code;
		this.points = points;
	}	
	
	public Card() {	
	}
	
	// id
	public void setId(int id) {
		this.id = id;
	}
	public int getId() {
		return this.id;
	}

	// code
	public void setCode(String code) {
		this.code = code;
	}
	public String getCode() {
		return this.code;
	}
	
	// point
	public void setPoints(int points) {
		this.points = points;
	}
	public int getPoints() {
		return this.points;
	}
		
	@Override
	public String toString() {
		return "Card [" + "id=" + this.id + ", code=" + this.code + ", points=" + this.points + "]";
	}
}