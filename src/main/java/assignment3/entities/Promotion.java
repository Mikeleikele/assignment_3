package assignment3.entities;


import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "promotion")
public class Promotion {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "promotion_id")
	private int id;
	
	@Column(name = "name")
	private String name  ;
	
	@Column(name = "discount")
	private int discount;
	
	
	// define cascade
	@OneToMany(mappedBy="promotion", fetch  = FetchType.EAGER, cascade = CascadeType.REMOVE , targetEntity = Scontrino.class, orphanRemoval = true)
    private Set<Scontrino> promotion_scontrino = new HashSet<Scontrino>();
	public void addOrder(Scontrino order) {
		this.promotion_scontrino.add(order);
	}	
	public Set<Scontrino> getListOrder() {
		return this.promotion_scontrino;
	}
		
	public Promotion(int id, String name, int discount) {
		this.id = id;
		this.name = name;
		this.discount = discount;		
	}

	public Promotion() {		
	}
		
	// id
	public void setId(int id) {
		this.id = id;
	}
	public int getId() {
		return this.id;
	}

	// name
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return this.name;
	}
	
	// discount
	public void setDiscount(int discount) {
		this.discount = discount;
	}
	public int getDiscount() {
		return this.discount;
	}
	
	@Override
	public String toString() {
		return "Promotion [" + "id=" + this.id + ", name=" + this.name + ", discount=" + this.discount + "]";
	}
}