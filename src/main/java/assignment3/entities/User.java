package assignment3.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "user")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_id")
	private int id;
	
	@Column(name = "mail")
	private String mail  ;
	
	@Column(name = "name")
	private String name;
	
	@Column(name= "surname")
	private String surname;
	
	@Column(name= "city")
	private String city;
	
	@Column(name= "street")
	private String street;
	
	@Column(name= "civicNumber")
	private String civicNumber;
	
	@OneToMany(mappedBy="user", fetch  = FetchType.EAGER, cascade = CascadeType.REMOVE, targetEntity = Scontrino.class, orphanRemoval = true)
    private Set<Scontrino> user_scontrino = new HashSet<Scontrino>();	
	public void addOrder(Scontrino order) {
		this.user_scontrino.add(order);
	}	
	public Set<Scontrino> getListOrder() {
		return this.user_scontrino;
	}
	
	@OneToMany(mappedBy="user", fetch  = FetchType.EAGER, cascade = CascadeType.REMOVE, targetEntity = Card.class, orphanRemoval = true)
    private Set<Card> user_card = new HashSet<Card>();	
	public void addCard(Card card) {
		this.user_card.add(card);
	}	
	public Set<Card> getListCard() {
		return this.user_card;
	}
		
	public User(int id, String mail, String name, String surname, String city, String street, String civicNumber) {
		super();
		this.id = id;
		this.mail = mail;
		this.name = name;
		this.surname = surname;
		this.city = city;
		this.street = street;
		this.civicNumber = civicNumber;
	}

	public User() {
	}
	
	// id
	public void setId(int id) {
		this.id = id;
	}
	public int getId() {
		return this.id;
	}
	
	// id
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getMail() {
		return this.mail;
	}

	// name
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return this.name;
	}
	
	// surname
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getSurname() {
		return this.surname;
	}
	
	// city
	public void setCity(String city) {
		this.city = city;
	}
	public String getCity() {
		return this.city;
	}
	
	// street
	public void setStreet(String street) {
		this.street = street;
	}
	public String getStreet() {
		return this.street;
	}

	// civicNumber
	public void setCivicNumber(String civicNumber) {
		this.civicNumber = civicNumber;
	}
	public String getCivicNumber() {
		return this.civicNumber;
	}
	
	// address
	public void setAddress(String city, String street, String civicNumber) {
		this.city = city;
		this.street = street;
		this.civicNumber = civicNumber;
	}
	public String getAddress() {
		return city+" "+street+" "+civicNumber;
	}
	
	@Override
	public String toString() {
		return "User [" + "id=" + this.id + ", name=" + this.name + ", surname=" + this.surname + ", city=" + this.city + ", street=" + this.street + ", civicNumber=" + this.civicNumber + "]";
	}
}