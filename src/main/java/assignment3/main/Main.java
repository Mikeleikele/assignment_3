package assignment3.main;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import assignment3.dao.ScontrinoDao;
import assignment3.dao.UserDao;
import assignment3.entities.Promotion;
import assignment3.entities.Scontrino;
import assignment3.entities.User;

public class Main {

	private static final EntityManagerFactory ENTITY_MANAGER_FACTORY = Persistence.createEntityManagerFactory("pss_assignment3");

	public static void main(String[] args) {	
			
			UserDao.create(1, "a_mail", "a_name", "a_surname", "a_city", "a_street", "a_civicNumber");
			UserDao.create(2, "b_mail", "b_name", "b_surname", "b_city", "b_street", "b_civicNumber");
			UserDao.create(3, "c_mail", "c_name", "c_surname", "c_city", "c_street", "c_civicNumber");
			UserDao.create(4, "d_mail", "d_name", "d_surname", "d_city", "d_street", "d_civicNumber");
			UserDao.create(5, "no_mail", "no_name", "no_surname", "no_city", "no_street", "no_civicNumber");
			
			
			List<User> user1 = UserDao.readAll();
			for (User b : user1) {
				System.out.println("-" + b.toString());
			}
			
			ScontrinoDao.create(1, 500);
			ScontrinoDao.create(2, 150);
			ScontrinoDao.create(3, 1500);
		
			UserDao.addOrder(3,ScontrinoDao.findById(1));
			ScontrinoDao.addUser(1, UserDao.findById(3));
			
			UserDao.addOrder(3,ScontrinoDao.findById(2));
			ScontrinoDao.addUser(2, UserDao.findById(3));
			
			UserDao.addOrder(4,ScontrinoDao.findById(3));
			ScontrinoDao.addUser(3, UserDao.findById(4));
			
			List<User> user_2 = UserDao.readAll();
			for (User b : user_2) {
				System.out.println("----------USER:\t" + b.toString());
				Set<Scontrino> _sc = b.getListOrder();
				for (Scontrino a : _sc) {
					System.out.println("Scontrino\t\t" + a.toString());
					Promotion pr = a.getPromotion();
					if (pr!=null)
					{
						System.out.println("Promo\t\t" + pr.toString());
						System.out.println(pr.getListOrder().toString());
					}
				}
			}
		ENTITY_MANAGER_FACTORY.close();
		System.exit(0);
	}
}

