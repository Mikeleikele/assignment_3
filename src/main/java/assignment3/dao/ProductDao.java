package assignment3.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import assignment3.dao.Helper;
import assignment3.entities.Product;
import assignment3.entities.Scontrino;


public class ProductDao {
		
	public ProductDao() {
	}

	public static void create(int id, String name, String description) {
		EntityManager manager = Helper.ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction transaction = null;
		try {
			transaction = manager.getTransaction();
			transaction.begin();
			Product product = new Product(id,name,description);
			manager.merge(product);
			transaction.commit();
		} catch (Exception ex) {
			if (transaction != null) {
				transaction.rollback();
			}
			ex.printStackTrace();
		} finally {
			manager.close();
		}
	}
	
	public static List<Product> readAll() {
		List<Product> product = null;
		EntityManager manager = Helper.ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction transaction = null;
		try {
			transaction = manager.getTransaction();
			transaction.begin();
			product = manager.createQuery("SELECT s FROM Product s", Product.class).getResultList();

			transaction.commit();
		} catch (Exception ex) {
			if (transaction != null) {
				transaction.rollback();
			}
			ex.printStackTrace();
		} finally {
			manager.close();
		}
		return product;
	}
	
	public static void delete(int id) {
		EntityManager manager = Helper.ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction transaction = null;
		try {
			transaction = manager.getTransaction();
			transaction.begin();
			Product product = manager.find(Product.class, id);			
			manager.remove(product);
			transaction.commit();
		} catch (Exception ex) {
			if (transaction != null) {
				transaction.rollback();
			}
			ex.printStackTrace();
		} finally {
			manager.close();
		}
	}
	
	public static void update(int id, String name,String description) {
		EntityManager manager = Helper.ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction transaction = null;
		try {
			transaction = manager.getTransaction();
			transaction.begin();
			Product product = manager.find(Product.class, id);
			product.setId(id);
			product.setName(name);
			product.setDescription(description);
			manager.persist(product);
			transaction.commit();
		} catch (Exception ex) {
			if (transaction != null) {
				transaction.rollback();
			}
			ex.printStackTrace();
		} finally {
			manager.close();
		}
	}
	
	public static Product findById(int id) {
		EntityManager manager = Helper.ENTITY_MANAGER_FACTORY.createEntityManager();
		Product product = manager.find(Product.class, id);
		if(product!= null) {
			return product;
		}else {
			return null;
		}
	}
	
	public static void addProduct(int id,Product productAdd) {
		EntityManager manager = Helper.ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction transaction = null;
		try {
			transaction = manager.getTransaction();
			transaction.begin();
			Product product = manager.find(Product.class, id);
			product.addProduct(productAdd);
			manager.persist(product);	
			transaction.commit();
		} catch (Exception ex) {
			if (transaction != null) {
				transaction.rollback();
			}
			ex.printStackTrace();
		} finally {
			manager.close();
		}
	}

	public static void addParent(int id,Product productAdd) {
		EntityManager manager = Helper.ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction transaction = null;
		try {
			transaction = manager.getTransaction();
			transaction.begin();
			Product product = manager.find(Product.class, id);
			product.setParent(productAdd);
			manager.persist(product);	
			transaction.commit();
		} catch (Exception ex) {
			if (transaction != null) {
				transaction.rollback();
			}
			ex.printStackTrace();
		} finally {
			manager.close();
		}
	}
	
	public static void addScontrino(int id,Scontrino scontrino) {
		EntityManager manager = Helper.ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction transaction = null;
		try {
			transaction = manager.getTransaction();
			transaction.begin();
			Product product = manager.find(Product.class, id);
			product.setScontrino(scontrino);
			manager.persist(product);	
			transaction.commit();
		} catch (Exception ex) {
			if (transaction != null) {
				transaction.rollback();
			}
			ex.printStackTrace();
		} finally {
			manager.close();
		}
	}

	public void deleteAll() {
		List<Product> entityList = readAll();
		for (Product entity : entityList) {
			delete(entity.getId());
		}
	}

}
