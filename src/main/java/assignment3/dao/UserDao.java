package assignment3.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import assignment3.dao.Helper;
import assignment3.entities.Card;
import assignment3.entities.Scontrino;
import assignment3.entities.User;

public class UserDao {
		
	public UserDao() {
	}

	public static void create(int id, String mail, String name, String surname, String city, String street, String civicNumber) {
		EntityManager manager = Helper.ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction transaction = null;
		try {
			transaction = manager.getTransaction();
			transaction.begin();
			User user = new User(id,mail,name,surname,city, street, civicNumber);
			manager.merge(user);
			transaction.commit();
		} catch (Exception ex) {
			if (transaction != null) {
				transaction.rollback();
			}
			ex.printStackTrace();
		} finally {
			manager.close();
		}
	}
	
	public static List<User> readAll() {
		List<User> users = null;
		EntityManager manager = Helper.ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction transaction = null;
		try {
			transaction = manager.getTransaction();
			transaction.begin();
			users = manager.createQuery("SELECT s FROM User s", User.class).getResultList();
			transaction.commit();
		} catch (Exception ex) {
			if (transaction != null) {
				transaction.rollback();
			}
			ex.printStackTrace();
		} finally {
			manager.close();
		}
		return users;
	}

	public static void delete(int id) {
		EntityManager manager = Helper.ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction transaction = null;
		try {
			transaction = manager.getTransaction();
			transaction.begin();
			User user = manager.find(User.class, id);
			manager.remove(user);
			transaction.commit();
		} catch (Exception ex) {
			if (transaction != null) {
				transaction.rollback();
			}
			ex.printStackTrace();
		} finally {
			manager.close();
		}
	}
	
	public static void update(int id, String mail, String name, String surname, String city, String street, String civicNumber) {
		EntityManager manager = Helper.ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction transaction = null;
		try {
			transaction = manager.getTransaction();
			transaction.begin();
			User user = manager.find(User.class, id);
			user.setId(id);
			user.setMail(mail);
			user.setName(name);
			user.setSurname(surname);
			user.setAddress(city, street, civicNumber);
			manager.persist(user);
			transaction.commit();
		} catch (Exception ex) {
			if (transaction != null) {
				transaction.rollback();
			}
			ex.printStackTrace();
		} finally {
			manager.close();
		}
	}
	
	public static void addOrder(int id,Scontrino order) {
		EntityManager manager = Helper.ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction transaction = null;
		try {
			transaction = manager.getTransaction();
			transaction.begin();			
			User user = manager.find(User.class, id);
			user.addOrder(order);
			manager.persist(user);
			transaction.commit();
		} catch (Exception ex) {
			if (transaction != null) {
				transaction.rollback();
			}
			ex.printStackTrace();
		} finally {
			manager.close();
		}
	}
	
	public static void addCard(int id,Card card) {
		EntityManager manager = Helper.ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction transaction = null;
		try {
			transaction = manager.getTransaction();
			transaction.begin();			
			User user = manager.find(User.class, id);
			user.addCard(card);
			manager.persist(user);
			transaction.commit();
		} catch (Exception ex) {
			if (transaction != null) {
				transaction.rollback();
			}
			ex.printStackTrace();
		} finally {
			manager.close();
		}
	}
	
	public static void updateAddress(int id, String city, String street, String civicNumber) {
		EntityManager manager = Helper.ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction transaction = null;
		try {
			transaction = manager.getTransaction();
			transaction.begin();
			User user = manager.find(User.class, id);
			user.setAddress(city, street, civicNumber);			
			manager.persist(user);
			transaction.commit();
		} catch (Exception ex) {
			if (transaction != null) {
				transaction.rollback();
			}
			ex.printStackTrace();
		} finally {
			manager.close();
		}
	}
	
	public static User findById(int id) {
		EntityManager manager = Helper.ENTITY_MANAGER_FACTORY.createEntityManager();
		User user = manager.find(User.class, id);
		if(user!= null) {
			return user;
		}else {
			return null;
		}
	}

	public static void deleteAll() {
		List<User> entityList = readAll();
		for (User entity : entityList) {
			delete(entity.getId());
		}
	}
}
