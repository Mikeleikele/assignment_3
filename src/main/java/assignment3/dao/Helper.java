package assignment3.dao;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Helper {
	static final EntityManagerFactory ENTITY_MANAGER_FACTORY = Persistence.createEntityManagerFactory("pss_assignment3");
}
