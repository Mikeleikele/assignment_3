package assignment3.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import assignment3.dao.Helper;
import assignment3.entities.Promotion;
import assignment3.entities.Scontrino;

public class PromotionDao {
		
	public PromotionDao() {
	}

	public static void create(int id, String name, int discount) {
		EntityManager manager = Helper.ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction transaction = null;
		try {
			transaction = manager.getTransaction();
			transaction.begin();
			Promotion promotion = new Promotion(id,name,discount);
			manager.merge(promotion);
			transaction.commit();
		} catch (Exception ex) {
			if (transaction != null) {
				transaction.rollback();
			}
			ex.printStackTrace();
		} finally {
			manager.close();
		}
	}
	
	public static List<Promotion> readAll() {
		List<Promotion> promotion = null;
		EntityManager manager = Helper.ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction transaction = null;
		try {
			transaction = manager.getTransaction();
			transaction.begin();
			promotion = manager.createQuery("SELECT s FROM Promotion s", Promotion.class).getResultList();
			transaction.commit();
		} catch (Exception ex) {
			if (transaction != null) {
				transaction.rollback();
			}
			ex.printStackTrace();
		} finally {
			manager.close();
		}
		return promotion;
	}

	public static void delete(int id) {
		EntityManager manager = Helper.ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction transaction = null;
		try {
			transaction = manager.getTransaction();
			transaction.begin();
			Promotion promotion = manager.find(Promotion.class, id);			
			manager.remove(promotion);
			transaction.commit();
		} catch (Exception ex) {
			if (transaction != null) {
				transaction.rollback();
			}
			ex.printStackTrace();
		} finally {
			manager.close();
		}
	}
	
	public static void update(int id, String name,int discount) {
		EntityManager manager = Helper.ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction transaction = null;
		try {
			transaction = manager.getTransaction();
			transaction.begin();
			Promotion promotion = manager.find(Promotion.class, id);
			promotion.setId(id);
			promotion.setName(name);
			promotion.setDiscount(discount);
			manager.persist(promotion);
			transaction.commit();
		} catch (Exception ex) {
			if (transaction != null) {
				transaction.rollback();
			}
			ex.printStackTrace();
		} finally {
			manager.close();
		}
	}
	
	public static Promotion findById(int id) {
		EntityManager manager = Helper.ENTITY_MANAGER_FACTORY.createEntityManager();
		Promotion promotion = manager.find(Promotion.class, id);
		if(promotion!= null) {
			return promotion;
		}else {
			return null;
		}
	}
	
	public static void addOrder(int id,Scontrino order) {
		EntityManager manager = Helper.ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction transaction = null;
		try {
			transaction = manager.getTransaction();
			transaction.begin();
			Promotion promotion = manager.find(Promotion.class, id);
			promotion.addOrder(order);
			manager.persist(promotion);	
			transaction.commit();
		} catch (Exception ex) {
			if (transaction != null) {
				transaction.rollback();
			}
			ex.printStackTrace();
		} finally {
			manager.close();
		}
	}
	
	public static void deleteAll() {
		List<Promotion> entityList = readAll();
		for (Promotion entity : entityList) {
			delete(entity.getId());
		}
	}
}
