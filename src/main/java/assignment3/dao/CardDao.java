package assignment3.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import assignment3.dao.Helper;
import assignment3.entities.Card;
import assignment3.entities.User;

public class CardDao {
		
	public CardDao() {
	}

	public static void create(int id, String code,int points) {
		EntityManager manager = Helper.ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction transaction = null;
		try {
			transaction = manager.getTransaction();
			transaction.begin();
			Card card = new Card(id,code,points);
			manager.merge(card);
			transaction.commit();
		} catch (Exception ex) {
			if (transaction != null) {
				transaction.rollback();
			}
			ex.printStackTrace();
		} finally {
			manager.close();
		}
	}
	
	public static List<Card> readAll() {
		List<Card> card = null;
		EntityManager manager = Helper.ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction transaction = null;
		try {
			transaction = manager.getTransaction();
			transaction.begin();
			card = manager.createQuery("SELECT s FROM Card s", Card.class).getResultList();
			transaction.commit();
		} catch (Exception ex) {
			if (transaction != null) {
				transaction.rollback();
			}
			ex.printStackTrace();
		} finally {
			manager.close();
		}
		return card;
	}

	public static void delete(int id) {
		EntityManager manager = Helper.ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction transaction = null;
		try {
			transaction = manager.getTransaction();
			transaction.begin();
			Card card = manager.find(Card.class, id);			
			manager.remove(card);
			transaction.commit();
		} catch (Exception ex) {
			if (transaction != null) {
				transaction.rollback();
			}
			ex.printStackTrace();
		} finally {
			manager.close();
		}
	}
	
	public static void update(int id, String code, int points) {
		EntityManager manager = Helper.ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction transaction = null;
		try {
			transaction = manager.getTransaction();
			transaction.begin();
			Card card = manager.find(Card.class, id);
			card.setId(id);
			card.setCode(code);
			card.setPoints(points);
			manager.persist(card);
			transaction.commit();
		} catch (Exception ex) {
			if (transaction != null) {
				transaction.rollback();
			}
			ex.printStackTrace();
		} finally {
			manager.close();
		}
	}
	
	public static Card findById(int id) {
		EntityManager manager = Helper.ENTITY_MANAGER_FACTORY.createEntityManager();
		Card card = manager.find(Card.class, id);
		if(card!= null) {
			return card;
		}else {
			return null;
		}
	}
	
	public static void addUser(int id,User user) {
		EntityManager manager = Helper.ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction transaction = null;
		try {
			transaction = manager.getTransaction();
			transaction.begin();
			Card card = manager.find(Card.class, id);
			card.setUser(user);
			manager.persist(card);	
			transaction.commit();
		} catch (Exception ex) {
			if (transaction != null) {
				transaction.rollback();
			}
			ex.printStackTrace();
		} finally {
			manager.close();
		}
	}

	public static void deleteAll() {
		List<Card> entityList = readAll();
		for (Card entity : entityList) {
			delete(entity.getId());
		}
	}
}