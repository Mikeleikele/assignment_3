package assignment3.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import assignment3.dao.Helper;
import assignment3.entities.Product;
import assignment3.entities.Promotion;
import assignment3.entities.Scontrino;
import assignment3.entities.User;

public class ScontrinoDao {
		
	public ScontrinoDao() {
	}

	public static void create(int id, int total) {
		EntityManager manager = Helper.ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction transaction = null;
		try {
			transaction = manager.getTransaction();
			transaction.begin();			
			Scontrino order= new Scontrino();
			order.setId(id);
			order.setTotal(total);
			manager.merge(order);
			transaction.commit();
		} catch (Exception ex) {
			if (transaction != null) {
				transaction.rollback();
			}
			ex.printStackTrace();
		} finally {
			manager.close();
		}
	}
	
	public static List<Scontrino> readAll() {
		List<Scontrino> scontrino = null;
		EntityManager manager = Helper.ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction transaction = null;
		try {
			transaction = manager.getTransaction();
			transaction.begin();
			scontrino = manager.createQuery("SELECT s FROM Scontrino s", Scontrino.class).getResultList();
			transaction.commit();
		} catch (Exception ex) {
			if (transaction != null) {
				transaction.rollback();
			}
			ex.printStackTrace();
		} finally {
			manager.close();
		}
		return scontrino;
	}

	public static void delete(int id) {
		EntityManager manager = Helper.ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction transaction = null;
		try {
			transaction = manager.getTransaction();
			transaction.begin();
			Scontrino order= manager.find(Scontrino.class, id);
			manager.remove(order);
			transaction.commit();
		} catch (Exception ex) {
			if (transaction != null) {
				transaction.rollback();
			}
			ex.printStackTrace();
		} finally {
			manager.close();
		}
	}
	
	public static void update(int id, int total) {
		EntityManager manager = Helper.ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction transaction = null;
		try {
			transaction = manager.getTransaction();
			transaction.begin();
			Scontrino order= manager.find(Scontrino.class, id);
			order.setId(id);
			order.setTotal(total);
			manager.persist(order);
			transaction.commit();
		} catch (Exception ex) {
			if (transaction != null) {
				transaction.rollback();
			}
			ex.printStackTrace();
		} finally {
			manager.close();
		}
	}
	
	public static Scontrino findById(int id) {
		EntityManager manager = Helper.ENTITY_MANAGER_FACTORY.createEntityManager();
		Scontrino order= manager.find(Scontrino.class, id);
		return order;
	}
	
	public static void addUser(int id,User user) {
		EntityManager manager = Helper.ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction transaction = null;

		try {
			transaction = manager.getTransaction();
			transaction.begin();			
			Scontrino order= manager.find(Scontrino.class, id);
			order.setUser(user);			
			manager.persist(order);			
			transaction.commit();
		} catch (Exception ex) {
			if (transaction != null) {
				transaction.rollback();
			}
			ex.printStackTrace();
		} finally {
			manager.close();
		}
	}
	
	
	public static void addPromotion(int id,Promotion promotion) {
		EntityManager manager = Helper.ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction transaction = null;
		try {
			transaction = manager.getTransaction();
			transaction.begin();			
			Scontrino order= manager.find(Scontrino.class, id);
			order.setPromotion(promotion);
			manager.persist(order);			
			transaction.commit();
		} catch (Exception ex) {
			if (transaction != null) {
				transaction.rollback();
			}
			ex.printStackTrace();
		} finally {
			manager.close();
		}
	}
	
	public static void addProduct(int id,Product product) {
		EntityManager manager = Helper.ENTITY_MANAGER_FACTORY.createEntityManager();
		EntityTransaction transaction = null;
		try {
			transaction = manager.getTransaction();
			transaction.begin();			
			Scontrino order= manager.find(Scontrino.class, id);
			order.addProduct(product);			
			manager.persist(order);			
			transaction.commit();
		} catch (Exception ex) {
			if (transaction != null) {
				transaction.rollback();
			}
			ex.printStackTrace();
		} finally {
			manager.close();
		}
	}

	public void deleteAll() {
		List<Scontrino> entityList = readAll();
		for (Scontrino entity : entityList) {
			delete(entity.getId());
		}
	}

}
